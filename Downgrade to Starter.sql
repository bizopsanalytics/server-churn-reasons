with    land_sen as (
        select  sen, 
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        group by 1
        )
select          b.quarter, count(a.sen) as downgrade_starter 
                from land_sen as a 
                left join public.sale as b on a.sen = b.sen
                where b.platform = 'Server'
                and b.license_level = 'Starter'
                and b.date >= a.min_date
group by 1
order by 1