select description
from playground_intsys.renewals_lost
where   description ilike '%language%'
        or description ilike '%english%'
        or description ilike '%korean%'
        or description ilike '%chinese%'
        or description ilike '%hindi%'
        or description ilike '%localised%'
        or description ilike '%localized%'
        or description ilike '%speak%'
        or description ilike '%limited English%'