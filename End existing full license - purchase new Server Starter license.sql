--identify the number of times a Starter license is created for the same base_product after its maximum renewal date.
with    
land_sen as (
        select  sen, 
                base_product,
                email_domain,
                quarter,
                min(date) as min_date
        from    public.sale
        where   license_level= 'Full'
        and     platform = 'Server'
        and     base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and     financial_year > 'FY2012'
        group by 1,2,3,4
        ),
renewed as (
        select  a.sen, 
                b.quarter,
                max(b.date) as max_date
        from    land_sen as a 
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Renewal'
        and     b.quarter > a.quarter
        group by 1,2
        order by 1
        ),
starter_land as (
        select  sen, 
                email_domain,
                base_product,
                quarter,
                min(date) as min_start
        from public.sale
        where sale_type = 'Starter'
        and platform = 'Server'
        group by 1,2,3,4
        )
select  a.quarter as land_quarter,
        c.quarter as max_renew_quarter,
        b.quarter as starter_land_quarter,
        Count(distinct a.sen) as affected_sen
from    land_sen as a
left join starter_land as b on a.email_domain = b.email_domain
left join renewed as c on a.sen = c.sen
where   a.base_product = b.base_product
and     a.min_date < b.min_start
and     b.min_start > c.max_date + interval '270' DAY
and     b.min_start < c.max_date + interval '635' DAY
and     a.quarter < c.quarter
group by 1,2,3
order by 1,2,3