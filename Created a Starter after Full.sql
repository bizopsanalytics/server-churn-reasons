--didnt renew full, but created a new Starter
with full_license as
(
select  email_domain,
        sen,
        base_product,
        financial_year,
        min(date) as min_date
from sale
where license_level = 'Full'
and platform = 'Server'
group by 1,2,3,4
),
starter_license as
(
select  email_domain,
        sen,
        base_product,
        financial_year,
        min(date) as min_date
from sale
where license_level = 'Starter'
and platform = 'Cloud'
group by 1,2,3,4
)
select b.financial_year, a.base_product, count(distinct a.email_domain)
from full_license as a 
left join starter_license as b on a.email_domain = b.email_domain and a.base_product = b.base_product
where a.min_date < b.min_date
group by 1,2